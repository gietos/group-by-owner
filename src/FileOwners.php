<?php

namespace App;

class FileOwners
{
    public static function groupByOwners(array $files)
    {
        $owners = [];
        foreach ($files as $file => $owner) {
            $owners[$owner][] = $file;
        }
        return $owners;
    }
}
