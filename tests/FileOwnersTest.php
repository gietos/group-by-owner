<?php

class FileOwnersTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider()
    {
        return [
            [['Input.txt' => 'Randy', 'Code.py' => 'Stan', 'Output.txt' => 'Randy'], ['Randy' => ['Input.txt', 'Output.txt'], 'Stan' => ['Code.py']]],
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testFileOwners($files, $result)
    {
        return $this->assertEquals($result, \App\FileOwners::groupByOwners($files));
    }
}
